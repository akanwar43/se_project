-- --------------------------------------------------------
-- Host:                         10.222.49.95
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for HospitalManage
CREATE DATABASE IF NOT EXISTS `HospitalManage` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `HospitalManage`;

-- Dumping structure for table HospitalManage.appointments
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctor` varchar(45) NOT NULL,
  `type` varchar(255) NOT NULL,
  `specialization` varchar(100) NOT NULL,
  `classification` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `pid` int(11) NOT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.appointments: ~2 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT INTO `appointments` (`id`, `doctor`, `type`, `specialization`, `classification`, `date`, `pid`, `status`) VALUES
	(8, 'phname2', 'Family Med', 'Adult', 'Anesthesia', '2021-11-23', 1, 'new'),
	(9, 'phname1', 'Family Med', 'Adult', 'Anesthesia', '2021-11-23', 1, 'completed');
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table HospitalManage.medicines
CREATE TABLE IF NOT EXISTS `medicines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `outcome` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.medicines: ~7 rows (approximately)
/*!40000 ALTER TABLE `medicines` DISABLE KEYS */;
INSERT INTO `medicines` (`id`, `name`, `outcome`) VALUES
	(1, 'm_name001', 'treatment efforts 001'),
	(2, 'm_name002', 'treatment efforts 002'),
	(3, 'm_name003', 'efforts003'),
	(4, 'm_name004', 'efforts003'),
	(5, 'm_name005', 'efforts003'),
	(6, 'm_name006', 'efforts003'),
	(7, 'm_name007', 'efforts003');
/*!40000 ALTER TABLE `medicines` ENABLE KEYS */;

-- Dumping structure for table HospitalManage.patient
CREATE TABLE IF NOT EXISTS `patient` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) NOT NULL DEFAULT '',
  `street` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `contry` varchar(45) NOT NULL,
  `gender` varchar(50) NOT NULL DEFAULT '0',
  `birthday` varchar(50) NOT NULL DEFAULT '',
  `nationality` varchar(45) NOT NULL,
  `children` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.patient: ~5 rows (approximately)
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` (`pid`, `username`, `password`, `email`, `phone`, `street`, `city`, `contry`, `gender`, `birthday`, `nationality`, `children`) VALUES
	(1, 'paname1', 'aaaaaa', 'paname1@patient.com', '111111111111', 'street name 11111', 'city name 1111', 'USA', 'male', '06January1991', 'Russian', '1'),
	(2, 'paname2', 'aaaaaa', 'paname2@patient.com', '222222222222', 'street name 22222', 'city name 22222', 'USA', 'male', '06January1992', 'Russian', '1'),
	(3, 'paname3', 'aaaaaa', 'paname3@patient.com', '333333333333', 'street name 33333', 'city name 33333', 'USA', 'male', '06January1993', 'Russian', '1'),
	(4, 'paname4', 'aaaaaa', 'paname4@patient.com', '444444444444', 'street name 44444', 'city name 44444', 'USA', 'male', '06January1994', 'Russian', '1'),
	(5, 'paname5', 'aaaaaa', 'paname5@patient.com', '555555555555', 'street name 55555', 'city name 55555', 'USA', 'male', '06January1995', 'Russian', '1');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;

-- Dumping structure for table HospitalManage.physicians
CREATE TABLE IF NOT EXISTS `physicians` (
  `idphysicians` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `street` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `contry` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `birthday` varchar(50) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `specialization` varchar(45) DEFAULT NULL,
  `classification` varchar(45) DEFAULT NULL,
  `rank` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idphysicians`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.physicians: ~5 rows (approximately)
/*!40000 ALTER TABLE `physicians` DISABLE KEYS */;
INSERT INTO `physicians` (`idphysicians`, `username`, `password`, `email`, `phone`, `street`, `city`, `contry`, `gender`, `birthday`, `type`, `specialization`, `classification`, `rank`) VALUES
	(6, 'phname1', 'aaaaaa', 'phname1@ph.com', '11111111111', 'street name 1', 'City name 1', 'Armenia', 'female', '04September1980', 'Family Med', 'Pediatric', 'Cardiology', '0'),
	(7, 'phname2', 'aaaaaa', 'phname1@ph.com', '22222222222', 'street name 2', 'City name 2', 'Armenia', 'female', '04September1982', 'Family Med', 'Adult', 'Anesthesia', '0'),
	(8, 'phname3', 'aaaaaa', 'phname1@ph.com', '333333333', 'street name 3', 'City name 2', 'Armenia', 'female', '04September1983', 'Family Med', 'Adult', 'Anesthesia', '0'),
	(9, 'phname4', 'aaaaaa', 'phname1@ph.com', '44444444444', 'street name 4', 'City name 2', 'Armenia', 'female', '04September1984', 'Family Med', 'Adult', 'Anesthesia', '0'),
	(10, 'phname5', 'aaaaaa', 'phname1@ph.com', '5555555555', 'street name 5', 'City name 2', 'Armenia', 'female', '04September1985', 'Family Med', 'Adult', 'Anesthesia', '0');
/*!40000 ALTER TABLE `physicians` ENABLE KEYS */;

-- Dumping structure for table HospitalManage.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(50) NOT NULL,
  `room` varchar(50) NOT NULL,
  `bed` varchar(50) NOT NULL,
  `price` int(11) DEFAULT '200',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.rooms: ~6 rows (approximately)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `build`, `room`, `bed`, `price`) VALUES
	(1, '1', '101', '001', 300),
	(2, '1', '101', '002', 200),
	(3, '1', '201', '003', 200),
	(4, '1', '201', '002', 200),
	(5, '2', '301', '001', 200),
	(7, '111', '111', '111', 123);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Dumping structure for table HospitalManage.treatment
CREATE TABLE IF NOT EXISTS `treatment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(50) NOT NULL,
  `doctor` varchar(50) NOT NULL,
  `type` varchar(255) NOT NULL,
  `specialization` varchar(100) NOT NULL,
  `classification` varchar(100) DEFAULT NULL,
  `treat_date` varchar(50) NOT NULL,
  `treat_type` varchar(50) NOT NULL,
  `medicines` varchar(100) DEFAULT NULL,
  `room_bed` varchar(100) DEFAULT NULL,
  `stay_period` int(11) unsigned zerofill DEFAULT NULL,
  `apid` int(11) DEFAULT NULL,
  `paid` int(11) DEFAULT NULL,
  `docid` int(11) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table HospitalManage.treatment: ~1 rows (approximately)
/*!40000 ALTER TABLE `treatment` DISABLE KEYS */;
INSERT INTO `treatment` (`id`, `patient_name`, `doctor`, `type`, `specialization`, `classification`, `treat_date`, `treat_type`, `medicines`, `room_bed`, `stay_period`, `apid`, `paid`, `docid`, `description`) VALUES
	(5, 'paname1', 'phname1', 'Family Med', 'Adult', 'Anesthesia', '2021-11-22', 'out-patient', 'm_name001', '', 00000000000, 7, 1, 6, 'this is test description for patient name 1'),
	(6, 'paname1', 'phname1', 'Family Med', 'Adult', 'Anesthesia', '2021-11-23', 'in-patient', 'm_name001', '1_101_002', 00000000003, 8, 1, 6, 'it\'s treatment description for patient name1 ， it\'s treatment description for patient');
/*!40000 ALTER TABLE `treatment` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;


## SQL table

The SQL table is called `User` in the `test` database,

![table structure](table_structure.png)

## Installation and execution

Install the dependencies:

```pip install -r requirements.txt```

Then, you can run the project:

```python run.py -r```

## testing

You can run the tests.

Then, open a new terminal and executing:

```pytest```

To-do list:
- add count to visitors
